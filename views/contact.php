<?php 
	require_once("menu.php"); 
?>
	<div class="container contactUs">
		<form class="form-group" method="post" action="contact.php" data-toggle="validator" >
			<label>Email</label>
			<input class="form-control" type="email" name="email" required/><br/>
			
			<label>Mobile</label>
			<input class="form-control"  type="number" name="mobNum" required/><br/>
			
			<label>Message</label>
			<textarea class="form-control"  name="message" rows="10" required></textarea><br/>
			
			<input class="btn btn-primary" type="submit" value="Submit" name="submit"/>
		</form>
	</div>	
	
	
<?php
	require_once("../controllers/contactCont.php"); //Contact Controller.
	require_once("footer.php");
?>
