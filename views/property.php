<?php 
	require_once("menu.php"); 
	require_once("../models/propertyMod.php");
?>
	<div class="container">
		<h1>Property List</h1>
		<!-- values jaqblu ma tad database (l order by tal sql == ASC or DESC) -->
		<form method="post" action="property.php">
			<select class="form-control" name="order">
				<option value="ASC">Ascending</option>
				<option value="DESC">Descending</option>
			</select><br/>
			
			<!-- values jaqblu ma tad database (il value se jithol gol sql tal order by column) -->
			<select class="form-control" name="column">
				<option value="t.town">Location</option>
				<option value="price">Price</option>
				<option value="typ.type">Propery Type</option>
			</select><br/>
			
			<input type="submit" value="Order By" class="btn btn-primary" name="orderBy" />
		</form>
		<br/>
		<table  class="myT table table-striped">
			<tr>
				<th>Property ID</th>
				<th>Location</th>
				<th>Price</th>
				<th>Property type</th>
				<th>Preview</th>
			</tr>
			
			<?php
				while($row = mysqli_fetch_assoc($result)){
			?>
					<tr class="list">
						<td><?php echo $row['propertyId']; ?></td>
						<td><?php echo $row['town']; ?></td>
						<td><?php echo "&euro;" . $row['price']; ?></td>
						<td><?php echo $row['type']; ?></td>
						<td><a href="propertyDetails.php?propertyId=<?php echo $row['propertyId']; ?>"><img class="img-responsive" src="<?php echo $row['propertyImg']; ?>"  alt="<?php echo $row['town']?>" /></a></td>
						<td class="deleteButtons"><a href="deletePage.php?propertyId=<?php echo $row['propertyId']; ?>" class="btn btn-danger">Delete</a></td>
						<td class="editButtons"><a href="editPage.php?propertyId=<?php echo $row['propertyId']; ?>&townId=<?php echo $row['townId']; ?>&typeId=<?php echo $row['typeId']?>&town=<?php echo $row['town']?>&type=<?php echo $row['type']?>" class="btn btn-primary">Update</a></td>
					</tr>
			<?php
				} 
			?>
		</table>
		<?php require_once("../controllers/menuCont.php"); ?>	
	</div>
<?php
	require_once("footer.php");
?>
