<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="server_side_assignment">
		<meta name="author" content="Sasha_Attard">

		<title>Server side Assignment</title>

		<link href="../css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
		<link href="../css/logo-nav.css" rel="stylesheet">	<!-- Custom CSS -->
		<link href="../css/main.css" rel="stylesheet"/>
		
		
		
		
	</head>
	
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top"><!-- Navigation -->
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--<a class="navbar-brand" href="#">
						<img src="http://placehold.it/150x50&text=Logo" alt="">
					</a>-->
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						 <li>
							<a href="index.php">Home</a>
						</li>
						<li>
							<a href="about.php">About Us</a>
						</li>
						<li>
							<a href="property.php">Property List</a>
						</li>
						<li>
							<a href="contact.php">Contact Us</a>
						</li>
						
						<li>
							<a href="login.php" id="login">Login</a>
						</li>
						
						<li>
							<a href="logout.php" id="logout">LogOut</a>
						</li>
						
						<li>
							<a href="insertProp.php" id="insert">Insert</a>  <!--Insert property-->
						</li>
					</ul>
				</div> <!-- /.navbar-collapse -->
			</div> <!-- /.container -->
		</nav>
		<?php require_once("../controllers/menuCont.php"); ?>		
	
		