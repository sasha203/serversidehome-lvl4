<?php 
	require_once("menu.php"); 
?>

	
	<div class="container aboutCont">
		<div class="container-fluid header">
			<h1>About Us</h1>
		</div>
		
		<div class="container-fluid logo">
			<img src="../images/forSale.png" alt="house_for_sale_logo"/>
		</div>
		
		
		<div class="container-fluid content">
			<p><b>About realestate.com.mt</b></p>
			<p>At realestate.com.mt, our purpose is to empower people by making property simple,
			efficient and stress free. Whether you’re just beginning your property journey or have had years of experience,
			realestate.com.mt is the number one place for people to come together to explore,
			research and share their passion for Maltese property.</p>
		</div>
	</div>
	
	
<?php
	require_once("footer.php");
?>
