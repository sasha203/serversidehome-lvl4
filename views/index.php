<?php 
		require_once("menu.php"); 
		require_once("../models/indexMod.php");
?>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1>Welcome, <?php if(isset($_SESSION['user']))echo"$_SESSION[user]"?></h1>
				</div>
			</div>
			
			<!--Namel last 3 inputted properties.-->
			<div class="row">
				<div class="col-lg-12">
					<table  class="myT table table-striped">
						<tr>
							<th>Property ID</th>
							<th>Location</th>
							<th>Price</th>
							<th>Property type</th>
							<th>Preview</th>
						</tr>

						<?php
							while($row = mysqli_fetch_assoc($showLatest)){
						?>
								<tr class="list">
									<td><?php echo $row['propertyId']; ?></td>
									<td><?php echo $row['town']; ?></td>
									<td><?php echo "&euro;" . $row['price']; ?></td>
									<td><?php echo $row['type']; ?></td>
									<td><a href="propertyDetails.php?propertyId=<?php echo $row['propertyId']; ?>"><img class="img-responsive" src="<?php echo $row['propertyImg']; ?>"  alt="<?php echo $row['town']?>"/></a></td>
								</tr>
							<?php
							} 
							?>
					</table>
				</div>
			</div>
		</div><!-- /.container -->
<?php
		require_once("footer.php");
?>
