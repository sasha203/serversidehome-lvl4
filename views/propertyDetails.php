<?php
	require_once("menu.php");
	require_once("../models/propertyDetailsMod.php");
?>
<div class="container pd">
	<table  class="myT table table-striped">
		<h1>Property Details</h1>
		<tr>
			<th>Property ID</th>
			<th>Price</th>
			<th>Type</th>
			<th>Location</th>
			<th>Preview</th>
		</tr>
		
		<!-- When clicking on image property will be displayed alone.-->
		<tr class="individual">
			<td><?php echo $row['propertyId']; ?></td>
			<td><?php echo "&euro;" . $row['price']; ?></td>
			<td><?php echo $row['type']; ?></td>
			<td><?php echo $row['town']; ?></td>
			<td class="indImg"><img class="img-responsive" src="<?php echo $row['propertyImg']; ?>" /></td>
		</tr>
	</table>
</div>
	


<?php
	require_once("footer.php");
?>