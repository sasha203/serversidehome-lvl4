<?php 
	require_once("menu.php"); 
?>

<div class="container loginCont">
	<form class="form-group" method="post" action="login.php" data-toggle="validator" >
		<label>Username</label>
		<input class="form-control" type="text" name="user" required /><br/> 

		<label>Password</label>
		<input class="form-control"  type="password" name="pass" required /><br/>

		<input class="btn btn-primary" type="submit" value="Login" name="login"/>
	</form>
</div>

<?php
	require_once("../controllers/loginCont.php");
	require_once("footer.php");
?>
