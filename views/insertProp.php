<?php
	require_once("menu.php");
	require_once("../models/getOptionsMod.php")
?>
	<div class="container">
		<h1>Insert Property</h1>
		<form enctype="multipart/form-data" method="POST" action="upload.php" class="form-group" data-toggle="validator" >
			<label>Location</label>
			<select class="form-control" name="pLocation">
				<?php 
				while($row = mysqli_fetch_assoc($showLoc)){
				?>
					<option value="<?php echo $row['townId']; ?> "><?php echo $row['town']; ?></option>
				<?php
				}
				?>
			</select><br/>
			
			<label>Price</label>
			<input type="number" name="pPrice" class="form-control" required /> <br/>
			
			
			<label>Property type</label>
			<select class="form-control" name="pType">
				<?php
				while($row = mysqli_fetch_assoc($showType)){
				?>
					<option value="<?php echo $row['typeId']; ?> "><?php echo $row['type']; ?></option>
				<?php
				}
				?>
			</select><br/>
			
			<input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
			<label>Upload Image</label>
			
			<input name="propImg" type="file" class="btn btn-default" required/> <br/>
			
			<input type="submit" name="add" value="Add Property" class="btn btn-primary" />
		</form>
	</div>


<?php
	require_once("footer.php");
?>