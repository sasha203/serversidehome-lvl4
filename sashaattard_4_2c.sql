-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jun 04, 2017 at 05:09 PM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sashaattard_4.2c`
--
CREATE DATABASE IF NOT EXISTS `sashaattard_4.2c` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sashaattard_4.2c`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `username`, `password`) VALUES
(8, 'admin', '$2y$10$gE4XHgzJrNU8LmVFGBy4Lewk/v044eRdNWXuvLHNa5pWrT.wf4tbe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `propertyId` int(11) NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `type` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `propertyImg` varchar(255) NOT NULL,
  PRIMARY KEY (`propertyId`),
  KEY `type` (`type`),
  KEY `locationId` (`locationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`propertyId`, `price`, `type`, `locationId`, `propertyImg`) VALUES
(3, 400000, 2, 1, '../images/valletta.jpg'),
(4, 200000, 1, 4, '../images/sliema.jpg'),
(5, 350000, 3, 4, '../images/qawra.jpg'),
(6, 950000, 4, 5, '../images/stpaul.jpg'),
(19, 300000, 1, 5, '../images/stpapp.jpg'),
(20, 170000, 1, 1, '../images/vallettaApart.jpg'),
(24, 350000, 1, 4, '../images/qawraApp.jpg'),
(26, 200000, 2, 8, '../images/birgu.png'),
(35, 200000, 2, 6, '../images/sen-hoc.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_town`
--

CREATE TABLE IF NOT EXISTS `tbl_town` (
  `townId` int(11) NOT NULL AUTO_INCREMENT,
  `town` varchar(255) NOT NULL,
  PRIMARY KEY (`townId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_town`
--

INSERT INTO `tbl_town` (`townId`, `town`) VALUES
(1, 'Valletta'),
(2, 'Sliema'),
(3, 'Attard'),
(4, 'Qawra'),
(5, 'St.paul'),
(6, 'Senglea'),
(7, 'Cospicua'),
(8, 'Birgu'),
(9, 'Kalkara'),
(10, 'Gzira'),
(11, 'Birkirkara\r\n'),
(12, 'Paola'),
(13, 'Fgura');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE IF NOT EXISTS `tbl_type` (
  `typeId` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`typeId`, `type`) VALUES
(1, 'Apartment'),
(2, 'House of Character'),
(3, 'Penthouse'),
(4, 'Villa/Bungalow');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_property`
--
ALTER TABLE `tbl_property`
  ADD CONSTRAINT `locationFK` FOREIGN KEY (`locationId`) REFERENCES `tbl_town` (`townId`),
  ADD CONSTRAINT `typeFk` FOREIGN KEY (`type`) REFERENCES `tbl_type` (`typeId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
