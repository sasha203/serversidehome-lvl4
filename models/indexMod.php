<?php
	require_once("functions.php");
	$conn = connectToDb();



	// Will gett all the properties order them in DESC order and show the first 3.
	//Every time a property is added it will be placed on top and will show with the other 2 after it.
	$showLast3 = "	SELECT * 
					FROM tbl_property p
					JOIN tbl_town t, tbl_type typ
					WHERE p.locationId = t.townId
					AND p.type = typ.typeId
					ORDER BY propertyId DESC
					LIMIT 3";
							
							
							
	$showLatest = mysqli_query($conn,$showLast3)
	or die ("Error in query: " . mysqli_error($conn));