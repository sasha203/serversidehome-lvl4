<?php
	require_once("functions.php");
	$conn = connectToDb();
	
	$pId = $_GET['propertyId'];
	
	//This query is required to show the location and type instead of their ids and also taking the user to an individual property according to the image clicked.
	$getIdQuery = " SELECT * 
					FROM tbl_property p
					JOIN tbl_town t, tbl_type typ
					WHERE p.locationId = t.townId
					AND p.type = typ.typeId AND propertyId = '$pId' ";
	
	$res = mysqli_query($conn,$getIdQuery)
	or die ("Error in query: " . mysqli_error($conn));
	
	//Not in a while loop since inside the property Details only 
	$row = mysqli_fetch_assoc($res);
?>