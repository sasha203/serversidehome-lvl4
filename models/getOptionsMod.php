<?php
	require_once("functions.php");
	$conn = connectToDb();
	
	//For location drop down inside insertProp.php
	$locQuery = "SELECT * FROM tbl_town";
	
	//Result
	$showLoc = mysqli_query($conn,$locQuery)
	or die ("Error in query: " . mysqli_error($conn));
	
	
	//For Type dropdown inside insertProp.php
	$typeQuery = "SELECT * FROM tbl_type";
	
	//Result	
	$showType = mysqli_query($conn,$typeQuery)
	or die ("Error in query: " . mysqli_error($conn));
?>