<?php 
	require_once("functions.php");
	$conn = connectToDb();
	
	//Getting Data form database.
	
	//Variables assigned for GET url data.
	$pId = $_GET['propertyId']; //The id of the property that was clicked.
	
	$tId = $_GET['townId']; //The town id of the property that was clicked.
	$fullTown = $_GET['town']; //full town value.
	
	$tyId = $_GET['typeId']; //The type id of the property that was clicked.
	$fullType = $_GET['type']; //Full property type value.
	
	//For location drop down inside editPage.php
	$locQuery = "SELECT * FROM tbl_town";
	
	//Result
	$showLoc = mysqli_query($conn,$locQuery)
	or die ("Error in query: " . mysqli_error($conn));
	
	
	
	//Will get all the records matching the current pId.
	$getDataQuery = "	SELECT * FROM tbl_property 
						WHERE  propertyId = $pId";
						
	$result = mysqli_query($conn,$getDataQuery)
	or die ("Error in query: " . mysqli_error($conn));
	
	
	
	//For Type dropdown inside editPage.php
	$typeQuery = "SELECT * FROM tbl_type";
	
	//Result	
	$showType = mysqli_query($conn,$typeQuery)
	or die ("Error in query: " . mysqli_error($conn));
	
	
	//Query of image
	if(isset($_POST['editButt'])){
		if(move_uploaded_file($_FILES['propImg']['tmp_name'], $preview)){		
			$addImg = "INSERT INTO tbl_property (propertyImg) VALUES('$preview')";

			$result = mysqli_query($conn,$addImg)
			or die ("Error in query: " . mysqli_error($conn));		
		}
		else{
			echo "Problem: Could not move file to destination directory";
		}
	}

?>